<?php

namespace App\Http\Controllers;

use App\Pertanyaan;
use Illuminate\Http\Request;

class QnaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pertanyaan::all();
        return view ('tes.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Pertanyaan::all();
        return view('tes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = $request->validate([
            'kategori' => 'required',
            'pertanyaan' => 'required',
            'gambar' => 'required'
         ]);
         Pertanyaan::create([
             'kategori' => $request->kategori,
             'pertanyaan' => $request->pertanyaan,
             'gambar' => $request->gambar
         ]);
         return redirect('/tes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Pertanyaan::find($id);
        return view('tes.show',compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail= Pertanyaan::find($id);
        return view('tes.edit',compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi dulu
        $validasi = $request->validate([
            'kategori' => 'required',
            'pertanyaan' => 'required'
        ]);

        // update dulu
        $update = Pertanyaan::find($id);
        $update->kategori =$request->kategori;
        $update->pertanyaan =$request->pertanyaan;
        $update->gambar =$request->gambar;
        $update->update();
        return redirect('/tes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cari = Pertanyaan::find($id);
        $cari->delete();
        return redirect('/tes');
    }
}
