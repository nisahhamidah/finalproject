<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JawabanController extends Controller
{
    public function store (Request $request)
    {
        
        $request->validate([
        'jawaban' => 'required',
        
     ]);
        $jawaban = new pertanyaan;
        $jawaban->jawaban = $request["jawaban"];
        $jawaban->save();

        return redirect('/jawaban');
    }

    public function index() {
        $jawaban = jawaban::all();
        return view ('jawaban.index', compact('posts'));
    }

    public function show ($jawaban_id)
{
    $jawaban = cast::where('id', $jawaban_id)->first();
    return view ('jawaban.show', compact('jawaban'));
}

public function edit ($pertanyaan)
{
    $jawaban = cast::where('id', $jawaban_id)->first();
    return view ('jawaban.edit', compact('jawaban'));
}
public function update (Request $request, $jawaban_id)
{
    $request->validate([
        'jawaban' => 'required',

    ]);

    $jawaban = jawaban::find($jawaban_id);
 
    $jawaban->jawaban = $request['jawaban'];
    
    
    $jawaban->save();
    return redirect ('/jawaban');
}
public function destroy($jawaban_id)
{
    $jawaban = jawaban::find($jawaban_id);

    $jawaban->delete();
    return redirect ('/jawaban');
}

}
