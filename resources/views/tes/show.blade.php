@extends('layout.master')
@section('nama','ini detail')
@section('konten')
    
@forelse ($data as $key=>$value)
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mb-5 mb-lg-0">
                        <div class="blog_left_sidebar">
                            <article class="blog_item">
                                <div class="blog_item_img">
                                    <img class="card-img rounded-0" src="{{$value->gambar}}" alt="">
                                    <a href="#" class="blog_item_date">
                                        {{$key + 1}}
                                        {{$value->kategori}}
                                    </a>
                                </div>
                                <div class="blog_details">
                                    <a class="d-inline-block" href="blog_details.html">
                                        <h2 class="blog-head" style="color: #2d2d2d;">Google inks pact for new 35-storey office</h2>
                                    </a>
                                    <p>{{$value->pertanyaan}}</p>
                                    <ul class="blog-info-link">
                                        <li><a href="#"><i class="fa fa-user"></i> {{$value->kategori}} </a></li>
                                        <li><a href="/komen"><i class="fa fa-comments"></i>Comments</a></li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            @endforelse  
        </section>
@endsection