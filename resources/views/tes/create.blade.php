@extends('layout.master')
@section('judul','Buat Pertanyaan')
@section('content')
<form action="/tes" method="post">
  @csrf
  <div class="form-group">
    <label for="nama">kategori</label>
    <input type="text" class="form-control" name="kategori" id="kategori">  
  </div>
  @error('kategori')
    <div class="alert alert-danger">{{$message}}</div>
  @enderror

      <div class="form-group">
        <label for="bio">pertanyaan</label>
        <input type="text" class="form-control" name="pertanyaan" id="pertanyaan">
      </div>
      @error('pertanyaan')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
        
            <div class="form-group">
            <label for="umur">gambar</label>
            <input type="text" class="form-control" name="gambar" id="gambar">
          </div>
          @error('gambar')
          <div class="alert alert-danger">{{$message}}</div>
        @enderror
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection