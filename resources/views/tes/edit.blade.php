@extends('layout.master')
@section('judul','edit pertanyaan')
@section('content')    
<div>
    <h2>Edit Post {{$detail->id}}</h2>
    <form action="/tes/{{$detail->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">kategori</label>
            <input type="text" class="form-control" name="kategori" value="{{$detail->kategori}}" id="nama" placeholder="Masukkan nama">
            @error('kategori')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">pertanyaan</label>
            <input type="text" class="form-control" name="pertanyaan"  value="{{$detail->pertanyaan}}"  id="pertanyaan" placeholder="Masukkan pertanyaan">
            @error('pertanyaan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">gambar</label>
            <input type="text" class="form-control" name="gambar"  value="{{$detail->gambar}}"  id="gambar" placeholder="Masukkan gambar">
            @error('gambar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection