<section class="slider-area ">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider slider-height d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-7 col-md-12">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInLeft" data-delay="0.2s">@yield('judul')</h1>
                                    <p data-animation="fadeInLeft" data-delay="0.4s">@yield('keterangan')</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </section>
        