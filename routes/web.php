<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@home');
Route::get('/qna',function(){
    return view('pertanyaan.index');
});
// CRUD pertanyaan
// cread
Route::get('/pertanyaan/create','PertanyaanController@create'); //form menuju form create
Route::post('/pertanyaan', 'PertanyaanController@store'); //route untuk menyimpan data ke database

// Read
Route::get('/pertanyaan','PertanyaanController@index'); //route list kategori
Route::get('/pertanyaan/{pertanyaan_id}','PertanyaanController@show'); //Route detail cast

// Update
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit'); //Route menuju form edit
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update'); //Route untu update berdasarkan id di database  

//Delete
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy'); //Route untuk hapus data di database

 // CRUD jawaban
// cread
Route::get('/jawaban/create','JawabanController@create'); //form menuju form create
Route::post('/jawaban', 'JawabanController@store'); //route untuk menyimpan data ke database

// Read
Route::get('/jawaban','JawabanController@index'); //route list kategori
Route::get('/jawaban/{jawaban_id}','JawabanController@show'); //Route detail cast

// Update
Route::get('/jawaban/{jawaban_id}/edit', 'JawabanController@edit'); //Route menuju form edit
Route::put('/jawaban/{jawaban_id}', 'JawabanController@update'); //Route untu update berdasarkan id di database  

//Delete
Route::delete('/jawaban/{jawaban_id}', 'JawabanController@destroy'); //Route untuk hapus data di database



// tes 
Route::get('/tes', 'QnaController@index');
Route::get('/tes/create', 'QnaController@create')->name('tes.create');
Route::post('/tes', 'QnaController@store')->name('tes.store');
Route::get('/tes/{id}', 'QnaController@show')->name('tes.show');
Route::get('/tes/{id}/edit', 'QnaController@edit')->name('tes.edit');
Route::put('/tes/{id}', 'QnaController@update')->name('tes.update');
Route::delete('/tes/{id}', 'QnaController@destroy')->name('tes.destroy');


